<?php
/**
 * @package contactsubmissions
 * @subpackage processors
 */
class ContactSubmissionCreateProcessor extends modObjectCreateProcessor {
    public $classKey = 'ContactSubmission';
    public $languageTopics = array('contactsubmissions:default');
    public $objectType = 'contactsubmissions.contactsubmission';

    public function beforeSave() {
        $name = $this->getProperty('name');
		$email = $this->getProperty('email');

        if ( empty($name) )
		{
            $this->addFieldError('name',$this->modx->lexicon('contactsubmissions.contactsubmission_err_ns_name'));
        }
//		else if ($this->doesAlreadyExist(array('email' => $email)) )
//		{
//            $this->addFieldError('email',$this->modx->lexicon('contactsubmissions.contactsubmission_err_ae'));
//        }
        return parent::beforeSave();
    }
}
return 'ContactSubmissionCreateProcessor';